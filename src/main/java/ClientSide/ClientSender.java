package ClientSide;

import java.io.*;
import ProtocolTools.Message;
import ProtocolTools.Protocol;

public class ClientSender extends Thread{
    private OutputStream out;
    public ClientSender(OutputStream out){
        this.out = out;
    }

    public void processMessage(Message m){
        Protocol p = new Protocol(m,(byte)1);
        sendMessage(p.getBytePacket());
    }

    private void sendMessage(byte[] bytePacket) {
        try {
            out.write(bytePacket);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
