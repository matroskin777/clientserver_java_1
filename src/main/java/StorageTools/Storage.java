package StorageTools;

import ProtocolTools.Message;
import ProtocolTools.Protocol;
import javafx.util.Pair;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.LinkedBlockingQueue;

public class Storage extends Thread{
    private static LinkedBlockingQueue<Pair> clientQuery;
    private DatabaseManager databaseManager;

    public Storage(){
        databaseManager = new DatabaseManager();
        clientQuery = new LinkedBlockingQueue<>();
        start();
    }

    public void run(){
        while(true){
            try {
                serve(clientQuery.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    //TODO for example
    private void serve(Pair take) {
        String query = (String)take.getKey();
        switch (query){
            case "Get all":
                ResultSet rs;
                try {
                    rs = databaseManager.readGoods();
                    generateResponse(formatRS(rs), (OutputStream)take.getValue());
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            break;


        }
    }

    private String formatRS(ResultSet rs) {
        String s = "";
        try {
            while (rs.next()) {
                s += rs.getString("goodName") + " " + rs.getString("goodCode") + " " + rs.getInt("priceForOne") + " " + rs.getInt("amountOnStorage");
                s += "\n";
            }
        }catch (SQLException e){
            System.err.println("Error");
        }
        return s;
    }

    private void generateResponse(String s, OutputStream os) {
        Message m = new Message(s,2,1000);
        Protocol p = new Protocol(m,(byte)1);
        try {
            os.write(p.getBytePacket());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public static LinkedBlockingQueue<Pair> getClientQuery() {
        return clientQuery;
    }
}
