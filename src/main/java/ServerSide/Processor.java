package ServerSide;

import ProtocolTools.Message;
import ProtocolTools.Protocol;
import StorageTools.Good;
import StorageTools.Storage;
import javafx.util.Pair;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.LinkedBlockingQueue;

public class Processor extends Thread {
    private LinkedBlockingQueue<String> commandsQueue;
    public boolean workFinished = false;
    private OutputStream os;

    public Processor(OutputStream os) {
        commandsQueue = new LinkedBlockingQueue<String>();
        this.os = os;
        start();
    }

    public void run() {
        try {
            while (!workFinished) {
                process(commandsQueue.take());
            }
            System.out.println("ServerSide.Processor dead");
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
    //TODO database connection
    private void process(String take) {
        Pair<String, OutputStream> clientQuery = new Pair<>(take,os);
        try {
            Storage.getClientQuery().put(clientQuery);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    public LinkedBlockingQueue<String> getCommandsQueue() {
        return commandsQueue;
    }
}
