import ClientSide.StoreClientUDP;
import ServerSide.StoreServerUDP;
import org.junit.Test;

import java.io.IOException;

public class UDPServerTest {
    @Test
    public void serveClients(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                StoreServerUDP ser = new StoreServerUDP();
                try {
                    StoreServerUDP.main(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
        try {
            StoreClientUDP client1 = new StoreClientUDP("Get");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            StoreClientUDP client2 = new StoreClientUDP("Put");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void sendingPackets(){
        try {
            StoreClientUDP client1 = new StoreClientUDP("Get");
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            StoreClientUDP client2 = new StoreClientUDP("Put");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void launchingServer(){
        StoreServerUDP ser = new StoreServerUDP();
        try {
            StoreServerUDP.main(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
