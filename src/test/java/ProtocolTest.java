import ProtocolTools.Message;
import ProtocolTools.Protocol;
import org.junit.Test;
public class ProtocolTest {

    @Test(expected = NullPointerException.class)
    public void constructorNoMessageExceptionTest(){
        Message m = null;
        Protocol p = new Protocol(m,(byte)1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorIllegalAppIdExceptionTest(){
        Message m = new Message("Hi",1,1);
        Protocol p = new Protocol(m,(byte)-1);
    }


}
