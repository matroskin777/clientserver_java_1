import ClientSide.Client;
import ServerSide.StoreServerTCP;
import org.junit.Test;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class TCPServerTest {
    @Test(expected = IllegalStateException.class)
    public void serverOffline(){
        InetAddress address = null;
        try {
            address = InetAddress.getByName(null);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
//        Client client = new Client(address,2,5);
//        Client client2 = new Client(address,3,2);
    }


    @Test
    public void serveClients(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                StoreServerTCP ser = new StoreServerTCP();
                try {
                    StoreServerTCP.main(null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();


        InetAddress address = null;
        try {
            address = InetAddress.getByName(null);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
//        Client client = new Client(address,2,5);
//        Client client2 = new Client(address,3,2);
    }

    @Test(expected = NullPointerException.class)
    public void incorrectIP(){
        InetAddress address = null;
//        Client client = new Client(address,2,5);
//        Client client2 = new Client(address,3,2);
    }
}
