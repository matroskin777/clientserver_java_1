package StorageTools;

import org.junit.Test;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatabaseManagerTest {

    @Test
    public void createGood() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.createGood(4,"Coca-Cola","CC",7,500);

        System.out.println("After creating new good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.deleteGoodById(4);
    }

    @Test
    public void readGoods() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        String s = formatRS(dm.readGoods());

        System.out.println("All table: ");
        System.out.println(s);
    }

    @Test
    public void updateGood() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.updateGood(2, "Milka","ML",30,100);

        System.out.println("After updating good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.updateGood(2,"Roshen","RS",25,150);
    }

    @Test
    public void addGoodAmountName() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.addGoodAmount("roshen",10);

        System.out.println("After adding 10 to good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.removeGoodAmount("roshen",10);
    }

    @Test
    public void addGoodAmountId() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.addGoodAmount(2,10);

        System.out.println("After adding 10 to good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.removeGoodAmount("roshen",10);
    }

    @Test
    public void removeGoodAmountName() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.removeGoodAmount("roshen",10);

        System.out.println("After removing 10 goods: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.addGoodAmount(2,10);

    }

    @Test
    public void removeGoodAmountId() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.removeGoodAmount(2,10);

        System.out.println("After removing 10 goods: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.addGoodAmount(2,10);

    }

    @Test
    public void deleteGoodById() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.deleteGoodById(2);

        System.out.println("After total removing good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.createGood(2,"Roshen","RS",25,150);
    }

    @Test
    public void deleteGoodByName() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        dm.deleteGoodByName("roshen");

        System.out.println("After total removing good: ");
        String s = formatRS(dm.readGoods());
        System.out.println(s);

        dm.createGood(2,"Roshen","RS",25,150);
    }

    @Test
    public void getGoodById() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        String s = formatRS(dm.getGoodById(3));

        System.out.println("Getting good of ID(3):");
        System.out.println(s);
    }

//    @Test
//    public void reserve() throws SQLException, ClassNotFoundException {
//        DatabaseManager dm = new DatabaseManager();
//        dm.deleteGoodById(100);
//    }

    @Test
    public void getGoodByName() throws SQLException, ClassNotFoundException {
        DatabaseManager dm = new DatabaseManager();
        String s = formatRS(dm.getGoodByName("milk"));

        System.out.println("Getting good of name(milk):");
        System.out.println(s);
    }


    private String formatRS(ResultSet rs) {
        String s = "";
        try {
            while (rs.next()) {
                s += rs.getString("goodName") + " " + rs.getString("goodCode") + " " + rs.getInt("priceForOne") + " " + rs.getInt("amountOnStorage");
                s += "\n";
            }
        }catch (SQLException e){
            System.err.println("Error");
        }
        return s;
    }
}