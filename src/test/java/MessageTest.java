import ProtocolTools.Message;
import org.junit.Assert;
import org.junit.Test;

import javax.crypto.SecretKey;

public class MessageTest {

    @Test(expected = IllegalArgumentException.class)
    public void constructorTypeExceptionTest(){
        Message m = new Message("Hello", 1,-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorUserIdExceptionTest(){
        Message m = new Message("Hello", 1,-1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructorEmptyMessageExceptionTest(){
        Message m = new Message("", 1,1);
    }

    @Test
    public void getSecretKeyIsKeyTest() {
        Assert.assertEquals(SecretKey.class,SecretKey.class);
    }

    @Test
    public void getResultMessageByteArrFillingTest(){
        Message m = new Message("Hi",1,1);
        assert(m.getResultMessageByteArr().length > 8);
    }
}