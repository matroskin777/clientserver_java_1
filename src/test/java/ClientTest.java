//import org.junit.Assert;
//import org.junit.Test;
//public class ClientTest {
//    @Test
//    public void putGoodsTest(){
//        StorageTools.Storage storage = new StorageTools.Storage();
//
//        //ServerSide.ServerReceiver and processor
//        ServerSide.Processor processor = new ServerSide.Processor();
//        ServerSide.ServerReceiver receiver = new ServerSide.ServerReceiver(processor);
//        Thread processorThread = new Thread(processor);
//        Thread receiverThread = new Thread(receiver);
//        processorThread.start();
//        receiverThread.start();
//
//        //Clients simulation (add 150)
//        MessageGenerator messageGeneratorPut = new MessageGenerator(receiver,2,50,50);
//        MessageGenerator messageGeneratorPut2 = new MessageGenerator(receiver,2,50,50);
//        MessageGenerator messageGeneratorPut3 = new MessageGenerator(receiver,2,50,50);
//
//        Thread client1 = new Thread(messageGeneratorPut);
//        Thread client2 = new Thread(messageGeneratorPut2);
//        Thread client3 = new Thread(messageGeneratorPut3);
//
//        client1.start();
//        client2.start();
//        client3.start();
//        try {
//            Thread.sleep(5000);
//            receiver.setRun(false);
//            processor.setRun(false);
//            client1.join();
//            client2.join();
//            client3.join();
//        }catch (InterruptedException ex){
//            System.out.println("InterruptedException");
//        }
//        Assert.assertEquals(150,StorageTools.Storage.milkStorage.size());
//    }
//
//    @Test
//    public void removeGoodsTest(){
//        StorageTools.Storage storage2 = new StorageTools.Storage();
//        try {
//            for (int i = 1; i <= 20; i++) {
//                storage2.getMilkStorage().put(new StorageTools.Good("Milk", "M"+i, 20.0));
//            }
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//
//        //ServerSide.ServerReceiver and processor
//        ServerSide.Processor processor = new ServerSide.Processor();
//        ServerSide.ServerReceiver receiver = new ServerSide.ServerReceiver(processor);
//        Thread processorThread = new Thread(processor);
//        Thread receiverThread = new Thread(receiver);
//        processorThread.start();
//        receiverThread.start();
//
//        //Clients simulation (remove 15)
//        MessageGenerator messageGeneratorRemove1 = new MessageGenerator(receiver,3,50,5);
//        MessageGenerator messageGeneratorRemove2 = new MessageGenerator(receiver,3,50,5);
//        MessageGenerator messageGeneratorRemove3 = new MessageGenerator(receiver,3,50,5);
//
//        Thread client1 = new Thread(messageGeneratorRemove1);
//        Thread client2 = new Thread(messageGeneratorRemove2);
//        Thread client3 = new Thread(messageGeneratorRemove3);
//
//        client1.start();
//        client2.start();
//        client3.start();
//        try {
//            Thread.sleep(3000);
//            receiver.setRun(false);
//            processor.setRun(false);
//            client1.join();
//            client2.join();
//            client3.join();
//        }catch (InterruptedException ex){
//            System.out.println("InterruptedException");
//        }
//        Assert.assertEquals(5,StorageTools.Storage.milkStorage.size());
//    }
//
//    @Test (expected = NullPointerException.class)
//    public void receiverNotRespondingException() {
//        StorageTools.Storage storage3 = new StorageTools.Storage();
//
//        //ServerSide.ServerReceiver(non-active) and processor
//        ServerSide.Processor processor = new ServerSide.Processor();
//        ServerSide.ServerReceiver receiver = null;
//        Thread processorThread = new Thread(processor);
//        processorThread.start();
//
//        //Clients simulation (add 5)
//        MessageGenerator messageGeneratorPut1 = new MessageGenerator(receiver,2,50,5);
//
//        Thread client1 = new Thread(messageGeneratorPut1);
//        client1.start();
//    }
//
//    @Test (expected = NullPointerException.class)
//    public void processorNotRespondingException() {
//        StorageTools.Storage storage4 = new StorageTools.Storage();
//
//        //ServerSide.ServerReceiver(non-active) and processor
//        ServerSide.Processor processor = null;
//        ServerSide.ServerReceiver receiver = new ServerSide.ServerReceiver(processor);
//        Thread processorThread = new Thread(processor);
//        processorThread.start();
//
//        //Clients simulation (add 5)
//        MessageGenerator messageGeneratorPut1 = new MessageGenerator(receiver,2,50,5);
//
//        Thread client1 = new Thread(messageGeneratorPut1);
//        client1.start();
//    }
//}
