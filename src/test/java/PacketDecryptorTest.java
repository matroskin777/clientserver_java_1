//import org.junit.Assert;
//import org.junit.Test;
//public class PacketDecryptorTest {
//    @Test(expected = IllegalArgumentException.class)
//    public void constructorInvalidDataFormatExceptionTest(){
//        ProtocolTools.Message m = new ProtocolTools.Message("Hello!",1,1);
//        byte[] invalidDataFormatArray = {'1','2','3'};
//        ProtocolTools.PacketDecryptor pd = new ProtocolTools.PacketDecryptor(invalidDataFormatArray,m.getSecretKey());
//    }
//
//    @Test
//    public void testMessageBody(){
//        ProtocolTools.Message m = new ProtocolTools.Message("Hello",1,1);
//        byte clientAppId = 1; // Optional
//        ProtocolTools.Protocol p = new ProtocolTools.Protocol(m,clientAppId);
//        ProtocolTools.PacketDecryptor pd = new ProtocolTools.PacketDecryptor(p.getBytePacket(),m.getSecretKey());
//        Assert.assertEquals("Hello",new String(pd.getMessage()));
//    }
//
//    @Test
//    public void testClientAppId(){
//        ProtocolTools.Message m = new ProtocolTools.Message("Hello",1,1);
//        byte clientAppId = 5; // Optional
//        ProtocolTools.Protocol p = new ProtocolTools.Protocol(m,clientAppId);
//        ProtocolTools.PacketDecryptor pd = new ProtocolTools.PacketDecryptor(p.getBytePacket(),m.getSecretKey());
//        Assert.assertEquals(5,pd.getUniqueClientAppId());
//    }
//
//    @Test
//    public void testCommandType(){
//        ProtocolTools.Message m = new ProtocolTools.Message("Hello",3,1);
//        byte clientAppId = 1; // Optional
//        ProtocolTools.Protocol p = new ProtocolTools.Protocol(m,clientAppId);
//        ProtocolTools.PacketDecryptor pd = new ProtocolTools.PacketDecryptor(p.getBytePacket(),m.getSecretKey());
//        Assert.assertEquals(3,pd.getCommandCode());
//    }
//
//    @Test
//    public void testUserId(){
//        ProtocolTools.Message m = new ProtocolTools.Message("Hello",1,1000);
//        byte clientAppId = 1; // Optional
//        ProtocolTools.Protocol p = new ProtocolTools.Protocol(m,clientAppId);
//        ProtocolTools.PacketDecryptor pd = new ProtocolTools.PacketDecryptor(p.getBytePacket(),m.getSecretKey());
//        Assert.assertEquals(1000,pd.getUserId());
//    }
//}
